import Vue from 'vue'
import App from './App.vue'

require('./assets/app.scss');

new Vue({
  el: '#app',
  render: h => h(App)
}).$mount("#app");
