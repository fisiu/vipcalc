# vipcalc
> VIP units calc

## Web
https://fisiu.gitlab.io/vipcalc/

## Build Setup

``` bash
# Install dependencies
yarn

# Serve with hot reload at localhost:8080
yarn dev

# Build for production with minification
yarn build
```
